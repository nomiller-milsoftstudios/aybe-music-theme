// External Dependencies
import React, { Component } from 'react';
import { v4 as uuidv4 } from 'uuid';

// Internal Dependencies
// import './style.css';
// import './js/jquery-2.1.4.min.js';
// import './script.js';


class MusicTile extends Component {

  static slug = 'abmt_music_tile';

  render() {
    const storeLogo = './images/store_logos/';
    const id = uuidv4();
    let listenNow;
    if (this.props.ln_show) {
      listenNow = 
        <div class="accordion" id={ "listen-now-" + id }>
          <div class="card">
            <div class="card-header x-now-header" id={ "listen-now-header-" + id }>
              <div class="row" data-toggle="collapse" data-target={ "#listen-now-body-" + id } aria-expanded="false" aria-controls={ "listen-now-body-" + id }>
                <div class="col-8">Listen Now</div>
                <div class="col-4 text-right"><span class="icon"></span></div>
              </div>
            </div>
            <div id={ "listen-now-body-" + id } class="collapse music-collapse" aria-labelledby={ "listen-now-header-" + id } data-parent={ "#listen-now-" + id }>
              <div class="card-body x-now-body text-center">
                <ul>

                  {this.props.ln_apple_music ?
                    <li>
                      <a href={this.props.ln_apple_music} target="_blank"><img src={ storeLogo + "apple-music.svg" } class="store-logo" alt="Apple Music" /></a>
                    </li>
                    : ''
                  }
                  
                  {this.props.ln_tidal ?
                    <li>
                      <a href={this.props.ln_tidal} target="_blank"><img src={ storeLogo + "tidal.svg" } class="store-logo" alt="Tidal" /></a>
                    </li>
                    : ''
                  }

                  {this.props.ln_soundcloud ?
                    <li>
                      <a href={this.props.ln_soundcloud} target="_blank"><img src={ storeLogo + "soundcloud.svg" } class="store-logo" alt="Soundcloud" /></a>
                    </li>
                    : ''
                  }
                  
                  {this.props.ln_spotify ?
                    <li>
                      <a href={this.props.ln_spotify} target="_blank"><img src={ storeLogo + "spotify.svg" } class="store-logo" alt="Spotify" /></a>
                    </li>
                    : ''
                  }
                  
                  {this.props.ln_youtube_music ?
                    <li>
                      <a href={this.props.ln_youtube_music} target="_blank"><img src={ storeLogo + "youtube-music.svg" } class="store-logo" alt="YouTube Music" /></a>
                    </li>
                    : ''
                  }
                  
                  {this.props.ln_pandora ?
                    <li>
                      <a href={this.props.ln_pandora} target="_blank"><img src={ storeLogo + "pandora.svg" } class="store-logo" alt="Pandora" /></a>
                    </li>
                    : ''
                  }
                  
                  {this.props.ln_audiomack &&
                    <li>
                      <a href={this.props.ln_audiomack} target="_blank"><img src={ storeLogo + "pandora.svg" } class="store-logo" alt="Pandora" /></a>
                    </li>
                  }
                  
                  {this.props.ln_deezer ?
                    <li>
                      <a href={this.props.ln_deezer} target="_blank"><img src={ storeLogo + "deezer.svg" } class="store-logo" alt="Deezer" /></a>
                    </li>
                    : ''
                  }
                  
                  {this.props.ln_genius ?
                    <li>
                      <a href={this.props.ln_genius} target="_blank"><img src={ storeLogo + "genius.svg" } class="store-logo" alt="Genius" /></a>
                    </li>
                    : ''
                  }

                </ul>
							</div>
						</div>
					</div>
				</div>;
    }

    let buyNow;
    if (this.props.bn_show) {
      buyNow =
        <div id={ "buy-now-" + id } class={ "accordion" + this.props.ln_show ? " buy-now-line-top" : "" }>
          <div class="card">
            <div class="card-header x-now-header" id={ "buy-now-header-" + id }>
              <div class="row" data-toggle="collapse" data-target={ "#buy-now-body-" + id } aria-expanded="false" aria-controls={ "buy-now-body-" + id }>
                <div class="col-8">Buy Now</div>
                <div class="col-4 text-right"><span class="icon"></span></div>
              </div>
            </div>
            <div id={ "buy-now-body-" + id } class="collapse music-collapse" aria-labelledby={ "buy-now-header-" + id } data-parent={ "#buy-now-" + id }>
              <div class="card-body x-now-body text-center">
                <ul>

                  {this.props.bn_itunes ?
                    <li>
                      <a href={this.props.bn_itunes} target="_blank"><img src={ storeLogo + "itunes.svg" } class="store-logo" alt="iTunes" /></a>
                    </li>
                    : ''
                  }

                  {this.props.bn_google_play ?
                    <li>
                      <a href={this.props.bn_google_play} target="_blank"><img src={ storeLogo + "google-play.svg" } class="store-logo" alt="Google Play" /></a>
                    </li>
                    :
                    ''
                  }

                  {this.props.bn_amazon_music ?
                    <li>
                      <a href={this.props.bn_amazon_music} target="_blank"><img src={ storeLogo + "amazon-music.svg" } class="store-logo" alt="Amazon Music" /></a>
                    </li>
                    : ''
                  }

                </ul>
							</div>
						</div>
					</div>
				</div>;
    }

    return (
      <div class="music-tile">
				<div class="music-thumbnail">
					<img class="img-fluid" src={this.props.cover_art} alt={this.props.song_title} />
				</div>
        
        {listenNow}
        
        {buyNow}

      </div>
    );
  }
}

export default MusicTile;
