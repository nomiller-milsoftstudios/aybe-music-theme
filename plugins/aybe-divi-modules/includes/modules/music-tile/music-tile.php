<?php


class ABMT_MusicTile extends ET_Builder_Module {

	public $slug       = 'abmt_music_tile';
	public $vb_support = 'on';

	protected $module_credits = array(
		'module_uri' => '',
		'author'     => 'MilSoft Studios',
		'author_uri' => 'https://milsoftstudios.com',
	);

	public function init() {
		$this->name = esc_html__( 'Music Tile', 'abmt-music-tile' );
	}

	public function get_fields() {
		$ln_tab_slug = 'listen_now';
		return array(
			'song_title' => array(
				'label'           => esc_html__( 'Song Title', 'abmt-music-tile' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'The song title.', 'abmt-music-tile' ),
				'toggle_slug'     => 'main_content',
			),
			'cover_art' => array(
				'label'              => esc_html__( 'Cover Art', 'abmt-music-tile' ),
				'type'               => 'upload',
				'option_category'    => 'basic_option', 
				'choose_text'        => esc_attr__('Choose an image','abmt-music-tile'),
				'upload_button_text' => esc_attr__('Upload an image','abmt-music-tile'),
				'description'        => esc_html__( 'The cover art.', 'abmt-music-tile' ),
				'data_type'          => 'image',
				'toggle_slug'        => 'main_content',
			),

			// Listen Now
			'ln_show' => array(
				'label'             => esc_html__( 'Show Listen Now', 'abmt-music-tile' ),
				'type'              => 'yes_no_button',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'abmt-music-tile' ),
					'off' => esc_html__( 'No', 'abmt-music-tile' ),
				),
				'toggle_slug'     => 'main_content',
			),
			'ln_apple_music' => array(
				'label'           => esc_html__( 'Listen Now: Apple Music', 'abmt-music-tile' ),
				'type'            => 'text',
				'option_category' => 'listen_now',
				'description'     => esc_html__( 'Apple Music link.', 'abmt-music-tile' ),
				'toggle_slug'     => 'main_content',
			),
			'ln_spotify' => array(
				'label'           => esc_html__( 'Listen Now: Spotify', 'abmt-music-tile' ),
				'type'            => 'text',
				'option_category' => 'listen_now',
				'description'     => esc_html__( 'Spotify link.', 'abmt-music-tile' ),
				'toggle_slug'     => 'main_content',
			),
			'ln_soundcloud' => array(
				'label'           => esc_html__( 'Listen Now: Soundcloud', 'abmt-music-tile' ),
				'type'            => 'text',
				'option_category' => 'listen_now',
				'description'     => esc_html__( 'Soundcloud link.', 'abmt-music-tile' ),
				'toggle_slug'     => 'main_content',
			),
			'ln_tidal' => array(
				'label'           => esc_html__( 'Listen Now: Tidal', 'abmt-music-tile' ),
				'type'            => 'text',
				'option_category' => 'listen_now',
				'description'     => esc_html__( 'Tidal link.', 'abmt-music-tile' ),
				'toggle_slug'     => 'main_content',
			),
			'ln_youtube_music' => array(
				'label'           => esc_html__( 'Listen Now: YouTube Music', 'abmt-music-tile' ),
				'type'            => 'text',
				'option_category' => 'listen_now',
				'description'     => esc_html__( 'YouTube Music link.', 'abmt-music-tile' ),
				'toggle_slug'     => 'main_content',
			),
			'ln_pandora' => array(
				'label'           => esc_html__( 'Listen Now: Pandora', 'abmt-music-tile' ),
				'type'            => 'text',
				'option_category' => 'listen_now',
				'description'     => esc_html__( 'Pandora link.', 'abmt-music-tile' ),
				'toggle_slug'     => 'main_content',
			),
			'ln_audiomack' => array(
				'label'           => esc_html__( 'Listen Now: Audiomack', 'abmt-music-tile' ),
				'type'            => 'text',
				'option_category' => 'listen_now',
				'description'     => esc_html__( 'Audiomack link.', 'abmt-music-tile' ),
				'toggle_slug'     => 'main_content',
			),
			'ln_deezer' => array(
				'label'           => esc_html__( 'Listen Now: Deezer', 'abmt-music-tile' ),
				'type'            => 'text',
				'option_category' => 'listen_now',
				'description'     => esc_html__( 'Deezer link.', 'abmt-music-tile' ),
				'toggle_slug'     => 'main_content',
			),
			'ln_genius' => array(
				'label'           => esc_html__( 'Listen Now: Genius', 'abmt-music-tile' ),
				'type'            => 'text',
				'option_category' => 'listen_now',
				'description'     => esc_html__( 'Genius link.', 'abmt-music-tile' ),
				'toggle_slug'     => 'main_content',
			),

			// Buy Now
			'bn_show' => array(
				'label'             => esc_html__( 'Show Buy Now', 'abmt-music-tile' ),
				'type'              => 'yes_no_button',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'abmt-music-tile' ),
					'off' => esc_html__( 'No', 'abmt-music-tile' ),
				),
				'toggle_slug'     => 'main_content',
			),
			'bn_itunes' => array(
				'label'           => esc_html__( 'Buy Now: iTunes', 'abmt-music-tile' ),
				'type'            => 'text',
				'option_category' => 'buy_now',
				'description'     => esc_html__( 'iTunes link.', 'abmt-music-tile' ),
				'toggle_slug'     => 'main_content',
			),
			'bn_google_play' => array(
				'label'           => esc_html__( 'Buy Now: Google Play', 'abmt-music-tile' ),
				'type'            => 'text',
				'option_category' => 'buy_now',
				'description'     => esc_html__( 'Google Play link.', 'abmt-music-tile' ),
				'toggle_slug'     => 'main_content',
			),
			'bn_amazon_music' => array(
				'label'           => esc_html__( 'Buy Now: Amazon Music', 'abmt-music-tile' ),
				'type'            => 'text',
				'option_category' => 'buy_now',
				'description'     => esc_html__( 'Amazon Music link.', 'abmt-music-tile' ),
				'toggle_slug'     => 'main_content',
			),
		);
	}

	public function render( $attrs, $content = null, $render_slug ) {
		$store_logo_dir = get_template_directory_uri() . '/images/store_logos/';
		$guid = $this->newGuid();

		$html = sprintf('
			<div class="music-tile">
				<div class="music-thumbnail">
					<img class="img-fluid" src="%2$s" alt="%1$s" />
				</div>',
			$this->props['song_title'],
			$this->props['cover_art']
		);

		// Listen Now
		if ( $this->props['ln_show'] ) {
			$html .= sprintf('
				<div class="accordion" id="listen-now-%1$s">
					<div class="card" style="border:none;">
						<div class="card-header x-now-header" id="listen-now-header-%1$s">
							<div class="row" data-toggle="collapse" data-target="#listen-now-body-%1$s" aria-expanded="false" aria-controls="listen-now-body-%1$s">
								<div class="col-8">Listen Now</div>
								<div class="col-4 text-right"><span class="icon"></span></div>
							</div>
						</div>
						<div id="listen-now-body-%1$s" class="collapse music-collapse" aria-labelledby="listen-now-header-%1$s" data-parent="#listen-now-%1$s">
							<div class="card-body x-now-body text-center">
								<ul>
			',
			$guid);

			if ( $this->props['ln_apple_music'] != '' ) {
				$html .= sprintf('
									<li>
										<a href="%1$s" target="_blank"><img src="' . $store_logo_dir . 'apple-music.svg" class="store-logo" alt="Apple Music" /></a>
									</li>
				',
				$this->props['ln_apple_music']);
			}

			if ( $this->props['ln_spotify'] != '' ) {
				$html .= sprintf('
									<li>
										<a href="%1$s" target="_blank"><img src="' . $store_logo_dir . 'spotify.svg" class="store-logo" alt="Spotify" /></a>
									</li>
				',
				$this->props['ln_spotify']);
			}
			
			if ( $this->props['ln_soundcloud'] != '' ) {
				$html .= sprintf('
									<li>
										<a href="%1$s" target="_blank"><img src="' . $store_logo_dir . 'soundcloud.svg" class="store-logo" alt="Soundcloud" /></a>
									</li>
				',
				$this->props['ln_soundcloud']);
			}

			if ( $this->props['ln_tidal'] != '' ) {
				$html .= sprintf('
									<li>
										<a href="%1$s" target="_blank"><img src="' . $store_logo_dir . 'tidal.svg" class="store-logo" alt="Tidal" /></a>
									</li>
				',
				$this->props['ln_tidal']);
			}

			if ( $this->props['ln_youtube_music'] != '' ) {
				$html .= sprintf('
									<li>
										<a href="%1$s" target="_blank"><img src="' . $store_logo_dir . 'youtube-music.svg" class="store-logo" alt="YouTube Music" /></a>
									</li>
				',
				$this->props['ln_youtube_music']);
			}

			if ( $this->props['ln_pandora'] != '' ) {
				$html .= sprintf('
									<li>
										<a href="%1$s" target="_blank"><img src="' . $store_logo_dir . 'pandora.svg" class="store-logo" alt="Pandora" /></a>
									</li>
				',
				$this->props['ln_pandora']);
			}

			if ( $this->props['ln_audiomack'] != '' ) {
				$html .= sprintf('
									<li>
										<a href="%1$s" target="_blank"><img src="' . $store_logo_dir . 'audiomack.svg" class="store-logo" alt="Audiomack" /></a>
									</li>
				',
				$this->props['ln_audiomack']);
			}

			if ( $this->props['ln_deezer'] != '' ) {
				$html .= sprintf('
									<li>
										<a href="%1$s" target="_blank"><img src="' . $store_logo_dir . 'deezer.svg" class="store-logo" alt="Deezer" /></a>
									</li>
				',
				$this->props['ln_deezer']);
			}

			if ( $this->props['ln_genius'] != '' ) {
				$html .= sprintf('
									<li>
										<a href="%1$s" target="_blank"><img src="' . $store_logo_dir . 'genius.png" class="store-logo" alt="Genius" /></a>
									</li>
				',
				$this->props['ln_genius']);
			}

			$html .= '
								</ul>
							</div>
						</div>
					</div>
				</div>';
		}

		// Buy Now
		if ( $this->props['bn_show'] ) {
			$html .= sprintf('
				<div class="accordion %2$s" id="buy-now-%1$s">
					<div class="card" style="border:none;">
						<div class="card-header x-now-header" id="buy-now-header-%1$s">
							<div class="row" data-toggle="collapse" data-target="#buy-now-body-%1$s" aria-expanded="false" aria-controls="buy-now-body-%1$s">
								<div class="col-8">Buy Now</div>
								<div class="col-4 text-right"><span class="icon"></span></div>
							</div>
						</div>
						<div id="buy-now-body-%1$s" class="collapse music-collapse" aria-labelledby="buy-now-header-%1$s" data-parent="#buy-now-%1$s">
							<div class="card-body x-now-body text-center">
								<ul>',
			$guid,
			$this->props['ln_show'] ? ' buy-now-line-top' : '');

			if ( $this->props['bn_itunes'] != '' ) {
				$html .= sprintf('
									<li>
										<a href="%1$s" target="_blank"><img src="' . $store_logo_dir . 'itunes.svg" class="store-logo" alt="Genius" /></a>
									</li>
				',
				$this->props['bn_itunes']);
			}

			if ( $this->props['bn_google_play'] != '' ) {
				$html .= sprintf('
									<li>
										<a href="%1$s" target="_blank"><img src="' . $store_logo_dir . 'google-play.svg" class="store-logo" alt="Genius" /></a>
									</li>
				',
				$this->props['bn_google_play']);
			}

			if ( $this->props['bn_amazon_music'] != '' ) {
				$html .= sprintf('
									<li>
										<a href="%1$s" target="_blank"><img src="' . $store_logo_dir . 'amazon-music.svg" class="store-logo" alt="Genius" /></a>
									</li>
				',
				$this->props['bn_amazon_music']);
			}

			$html .= '
								</ul>
							</div>
						</div>
					</div>
				</div>';
		}

		$html .= sprintf('
			</div>
		');

		return $html;
	}

	function newGuid()
	{
		if (function_exists('com_create_guid') === true)
		{
			return trim(com_create_guid(), '{}');
		}

		return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
	}
}

new ABMT_MusicTile;
