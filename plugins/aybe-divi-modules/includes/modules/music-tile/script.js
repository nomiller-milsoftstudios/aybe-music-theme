import $ from './js/jquery-2.1.4.min.js';

$(document).ready(function() {
    $('.collapse.music-collapse').on('show.bs.collapse', function(e) {
        var $card = $(this).closest('.music-tile');
        var $open = $($(this).data('parent')).find('.collapse.show');
    
        var additionalOffset = 0;
        if ($card.prevAll().filter($open.closest('.card')).length !== 0) {
            additionalOffset =  $open.height();
        }
        $('.page-content-col').animate({
            scrollTop: $card.offset().top - additionalOffset
        }, 250);
    });
});