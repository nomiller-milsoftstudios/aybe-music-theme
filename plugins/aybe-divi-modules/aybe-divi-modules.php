<?php
/*
Plugin Name: Ay.Be Divi Modules
Plugin URI:  
Description: Ay.Be Music custom Divi modules
Version:     1.0.0
Author:      MilSoft Studios
Author URI:  https://milsoftstudios.com
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: aybe-divi-modules
Domain Path: /languages

Ay.Be Divi Modules is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Ay.Be Divi Modules is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Music Tile. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
*/


if ( ! function_exists( 'abmt_initialize_extension' ) ):
/**
 * Creates the extension's main class instance.
 *
 * @since 1.0.0
 */
function abmt_initialize_extension() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/AybeDiviModules.php';
}
add_action( 'divi_extensions_init', 'abmt_initialize_extension' );
endif;
