$(document).ready(function() {
    var valid = 'invalid';
    function validateValue($value, $target, $placeholder, $email) {
        if ($email == true) {
            var n = $value.indexOf("@");
            var r = $value.lastIndexOf('.');
            if (n < 1 || r < n + 2 || r + 2 >= $value.length) {
                valid =  'invalid';
            } else {
                valid = 'valid';
            }
            
            if ($value == null || $value == '' || valid == 'invalid') {
                $target.addClass('visible');
            } else {
                $target.removeClass('visible');
            }

        } else {
            if ($value == null || $value == '' || $value == $placeholder) {
                $target.addClass('visible');
            } else {
                $target.removeClass('visible');
            }
        }
    };

    $('#mlSubmit').on('click', function(event) {
        var $this = $('.et-mailing-list-form');
        
        // 1. Prevent form submit default
        event.preventDefault();

        console.log('posted');

        // 2. serialize form data
        var formData = $this.serialize();

        var email   = $this.find('input[name="mlEmail"]');
        var fname   = $this.find('input[name="mlFirstName"]');
        var lname   = $this.find('input[name="mlLastName"]');
        var list    = $this.find('input[name="mlList"]');
        
        // 3. Before submit validate email
        validateValue(email.val(), email.next('.alert'), email.attr('data-placeholder'), true);

        if (email.val() != email.attr('data-placeholder') && valid == 'valid') {

            $this.find('.sending').addClass('visible');

            // 4. POST AJAX
            $.ajax({
                type: 'POST',
                url: $this.attr('action'),
                data: formData
            })
            .done(function(response) {
                // 5. If success show the success message to user
                $this.find('.sending').removeClass('visible');
                $this.find('.et-mailing-list-success').addClass('visible');
                setTimeout(function() {
                    $this.find('.et-mailing-list-success').removeClass('visible');
                    $("#mailingListModal").modal('hide');
                }, 2000);
            })
            .fail(function(data) {
                // 6. If fail show the error message to user
                $this.find('.sending').removeClass('visible');
                $this.find('.et-mailing-list-error').addClass('visible');
                setTimeout(function(){
                    $this.find('.et-mailing-list-error').removeClass('visible');
                }, 2000);
            })
            .always(function(){
                // 7. Clear the form fields for next subscibe request
                setTimeout(function(){
                    $this.find('input[name="mlEmail"]').val(email.attr('data-placeholder'));
                    $this.find('input[name="mlFirstName"]').val(fname.attr('data-placeholder'));
                    $this.find('input[name="mlLastName"]').val(lname.attr('data-placeholder'));
                }, 2000);
            });
        }
    });
});