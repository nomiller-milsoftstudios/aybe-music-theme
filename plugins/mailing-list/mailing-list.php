<?php
/*
    Plugin Name: Mailing List Signup
    Description: Mailing list subscribe (using MailChimp)
    Version: 1.0.1
    Author: MilSoft Studios
    Author URI: http://milsoftstudios.com
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// define( 'MAILCHIMP_API', '4cfeee540cdfc57f792e30f19209ecd5-us13' );
// define( 'MAILCHIMP_API', get_option( 'mailchimp_api_key' ) );

function register_mailing_list_script() {
	wp_register_script( 'widget-mailing-list', plugins_url( '/js/widget-mailing-list.js', __FILE__ ), array('jquery'), '', true );
	wp_register_style( 'widget-mailing-list', plugins_url( '/css/widget-mailing-list.css', __FILE__ ) );
}

add_action( 'wp_enqueue_scripts', 'register_mailing_list_script' );

function mailing_list_connect( $url, $api_key, $data = array() ) {

    $url .= '?' . http_build_query( $data );

    $mailchimp = curl_init();

    $headers = array(
		'Content-Type: application/json',
		'Authorization: Basic ' . base64_encode( 'user:' . $api_key )
	);

	curl_setopt( $mailchimp, CURLOPT_HTTPHEADER, $headers );
	curl_setopt( $mailchimp, CURLOPT_URL, $url );
	curl_setopt( $mailchimp, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $mailchimp, CURLOPT_CUSTOMREQUEST, 'GET' );
	curl_setopt( $mailchimp, CURLOPT_TIMEOUT, 10 );
	curl_setopt( $mailchimp, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0' );
	curl_setopt( $mailchimp, CURLOPT_SSL_VERIFYPEER, false );

    return curl_exec( $mailchimp );
}

function mailing_list_post( $email, $status, $list_id, $api_key, $merge_fields = array( 'FNAME' => '','LNAME' => '' ) ) {

    $data = array(
        'apikey'        => $api_key,
        'email_address' => $email,
        'status'        => $status,
        'merge_fields'  => $merge_fields
    );

    $url = 'https://' . substr($api_key, strpos($api_key,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . md5(strtolower($data['email_address']));

    $headers = array(
    	'Content-Type: application/json', 
    	'Authorization: Basic ' . base64_encode( 'user:'.$api_key )
	);

    $mailchimp = curl_init();
 
    curl_setopt( $mailchimp, CURLOPT_URL, $url );
    curl_setopt( $mailchimp, CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $mailchimp, CURLOPT_RETURNTRANSFER, true ); 
    curl_setopt( $mailchimp, CURLOPT_CUSTOMREQUEST, 'PUT' );
    curl_setopt( $mailchimp, CURLOPT_TIMEOUT, 10 );
    curl_setopt( $mailchimp, CURLOPT_POST, true );
    curl_setopt( $mailchimp, CURLOPT_POSTFIELDS, json_encode($data) );
    curl_setopt( $mailchimp, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0' );
    curl_setopt( $mailchimp, CURLOPT_SSL_VERIFYPEER, false );
 
    return curl_exec($mailchimp);
}

function mailing_list_list() {

    //$api_key = MAILCHIMP_API;
    $api_key = get_option( 'mailchimp_api_key' );

    if (empty($api_key)) {
        return new WP_Error( 'no_api_key', esc_html__( 'No Mailchimp API key is found.', 'mailing_list' ) );
    }

    if ( false === ( $mailchimp_list = get_transient( 'mailchimp-' . $api_key ) ) ) {

        $data = array(
            'fields' => 'lists',
            'count' => 'all',
        );

        $url = 'https://' . substr($api_key, strpos($api_key,'-') + 1) . '.api.mailchimp.com/3.0/lists/';
        $result = json_decode( mailing_list_connect( $url, $api_key, $data) );
       
        if (! $result ) {
            return new WP_Error( 'bad_json', esc_html__( 'Mailchimp has returned invalid data.', 'mailing_list' ) );
        }

        if ( !empty( $result->lists ) ) {
            foreach( $result->lists as $list ){
                $mailchimp_list[] = array(
                    'id'      => $list->id,
                    'name'    => $list->name,
                );
            }
        } else {
            return new WP_Error( 'no_list', esc_html__( 'Mailchimp did not return any list.', 'mailing_list' ) );
        }

        // do not set an empty transient - should help catch private or empty accounts.
        if ( ! empty( $mailchimp_list ) ) {
            $mailchimp_list = base64_encode( serialize( $mailchimp_list ) );
            set_transient( 'mailchimp-' . $api_key, $mailchimp_list, apply_filters( 'null_mailchimp_cache_time', WEEK_IN_SECONDS * 2 ) );
        }
    }

    if ( ! empty( $mailchimp_list ) ) {
        return unserialize( base64_decode( $mailchimp_list ) );
    } else {
        return new WP_Error( 'no_list', esc_html__( 'The mailing list did not return any lists.', 'mailing_list' ) );
    }
}

function mailing_list_action() {

    if ( ! isset( $_POST['et_mailing_list_nonce'] ) || !wp_verify_nonce( $_POST['et_mailing_list_nonce'], 'et_mailing_list_action' )) {
       exit;
    } else {

        $email     = filter_var(trim($_POST["mlEmail"]), FILTER_SANITIZE_EMAIL);
        $fname     = strip_tags(trim($_POST["mlFirstName"]));
        $lname     = strip_tags(trim($_POST["mlLastName"]));
        $list      = strip_tags(trim($_POST["mlList"]));
        // $api_key   = MAILCHIMP_API;
        $api_key = get_option( 'mailchimp_api_key' );

        mailing_list_post($email, 'subscribed', $list, $api_key, array('FNAME' => $fname,'LNAME' => $lname) );
        
        die;
    }
}
add_action('admin_post_nopriv_et_mailing_list', 'mailing_list_action');
add_action('admin_post_et_mailing_list', 'mailing_list_action');

class  WP_Widget_MailingList extends WP_Widget {

	public function __construct() {
		parent::__construct(
			'wpb_mailing_list',
			esc_html__('Mailing List Subscribe', 'mailing_list'),
			array( 'description' => esc_html__('Mailing list subscribe widget', 'mailing_list'))
        );
	}

	public function widget( $args, $instance ) {

		wp_enqueue_style('widget-mailing-list');
		wp_enqueue_script('widget-mailing-list');

        extract($args);
        
		$title  = apply_filters( 'widget_title', $instance['title'] ? $instance['title'] : 'Subscribe' );
		$list   = $instance['list'] ? esc_attr($instance['list']) : '';

		$output = '';
        $output .= $before_widget;
        
        $output .= '<div id="mailingListModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mailingListModalLabel">';
        $output .= '    <div class="modal-dialog modal-dialog-centered">';
        $output .= '        <div class="modal-content">';
        $output .= '            <div class="modal-header">';
        $output .= '                <h3 id="mailingListModalLabel" clas="modal-title">' . esc_html__($title, 'mailing_list') . '</h3>';
        $output .= '                <button type="button" class="close" data-dismiss="modal" aria-label="Close">';
        $output .= '                    <span aria-hidden="true">&times;</span>';
        $output .= '                </button>';
        $output .= '            </div>';
        $output .= '            <div class="modal-content border-0">';
        $output .= '                <div class="mailing-list-form mt-2 ml-3 mr-3">';
        
        $output .= '                    <form class="et-mailing-list-form" name="et-mailchimp-form" action="' . esc_url( admin_url('admin-post.php') ) . '" method="POST">';

        $output .= '                        <div class="form-group">';
        $output .= '                            <label for="mlEmail">Email address</label>';
        $output .= '                            <input type="text" class="form-control" value="" id="mlEmail" name="mlEmail" />';
        $output .= '                            <span class="alert warning">' . esc_html__('Invalid or empty email', 'mailing_list') . '</span>';
        $output .= '                        </div>';

        $output .= '                        <div class="form-group">';
        $output .= '                            <label for="mlFirstName">First Name</label>';
        $output .= '                            <input type="text" class="form-control" id="mlFirstName" name="mlFirstName" />';
        $output .= '                        </div>';

        $output .= '                        <div class="form-group">';
        $output .= '                            <label for="mlLastName">Last Name</label>';
        $output .= '                            <input type="text" class="form-control" id="mlLastName" name="mlLastName" />';
        $output .= '                        </div>';

        $output .='                         <div class="et-mailing-list-success alert final success">' . esc_html__('You have successfully subscribed to the newsletter.', 'mailing_list') . '</div>';
        $output .='                         <div class="et-mailing-list-error alert final error">' . esc_html__('Something went wrong. Your subscription failed.', 'mailing_list') . '</div>';
        
        $output .= '                        <input type="hidden" value="' . $list . '" name="mlList" />';
        $output .= '                        <input type="hidden" name="action" value="et_mailing_list" />';

        $output .= wp_nonce_field( "et_mailing_list_action", "et_mailing_list_nonce", false, false );

        $output .= '                    </form>';
        $output .= '                </div>';
        $output .= '            </div>';
        $output .= '            <div class="modal-footer">';
        $output .= '                <div class="send-div">';
        $output .= '                    <button type="button" class="site-button" data-dismiss="modal">Cancel</button>';
        $output .= '                    <input type="submit" class="site-button" value="' . esc_html__('Subscribe', 'mailing_list').'" name="mlSubmit" id="mlSubmit">';
        $output .= '                    <div class="sending"></div>';
        $output .= '                </div>';
        $output .= '            </div>';
        $output .= '        </div>';
        $output .= '    </div>';
        $output .= '</div>';

        
		$output .= $after_widget;
		echo $output;
	}

 	public function form( $instance ) {

		$defaults = array(
            'api_key' => get_option( 'mailchimp_api_key' ),
 			'title' => esc_html__('Subscribe', 'mailing_list'),
 			'list'  => '',
 		);

 		$instance = wp_parse_args((array) $instance, $defaults);

 		$list_array = mailing_list_list();

         ?>

			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php echo esc_html__( 'Title:', 'mailing_list' ); ?></label> 
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr($instance['title']); ?>" />
			</p>

			<?php if ( is_wp_error( $list_array ) ): ?>
				<p><?php echo wp_kses_post( $list_array->get_error_message() ); ?></p>
			<?php else: ?>
				<p>
					<label for="<?php echo $this->get_field_id( 'list' ); ?>"><?php echo esc_html__( 'List:', 'mailing_list' ); ?></label> 
					<select class="widefat" id="<?php echo $this->get_field_id( 'list' ); ?>" name="<?php echo $this->get_field_name( 'list' ); ?>" >
					<?php foreach ( $list_array as $list ) { ?>
						<option value="<?php echo $list['id']; ?>" <?php selected( $instance['list'], $list['id'] ); ?>><?php echo $list['name']; ?></option>
					<?php } ?>
					</select>
				</p>
			<?php endif; ?>
		
	<?php }

	public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;

		$instance['title']   = strip_tags( $new_instance['title'] );
        $instance['list']    = strip_tags( $new_instance['list'] );
        
		return $instance;
	}
}


function mailing_list_settings_page() {
	add_submenu_page(
		'options-general.php', // top level menu page
		'Mailing List Settings Page', // title of the settings page
		'Mailing List Settings', // title of the submenu
		'manage_options', // capability of the user to see this page
		'mailing-list-settings-page', // slug of the settings page
		'mailing_list_settings_page_html' // callback function to be called when rendering the page
	);
	add_action('admin_init', 'mailing_list_settings_init');
}
add_action('admin_menu', 'mailing_list_settings_page');

function mailing_list_settings_init() {
	add_settings_section(
		'mailing-list-settings-section', // id of the section
		'Mailing List Settings', // title to be displayed
		'', // callback function to be called when opening section
		'mailing-list-settings-page' // page on which to display the section, this should be the same as the slug used in add_submenu_page()
	);

	register_setting(
        'mailing-list-settings-page', // option group
        'mailchimp_api_key'
    );
    
    add_settings_field(
        'mailchimp-api-key', // id of the settings field
        'Mailchimp API Key', // title
        'mailing_list_settings_cb', // callback function
        'mailing-list-settings-page', // page on which settings display
        'mailing-list-settings-section' // section on which to show settings
    );
}

function mailing_list_settings_cb() {
	$api_key = esc_attr(get_option('mailchimp_api_key', ''));
	?>
    <div id="titlediv">
        <input id="title" type="text" name="mailchimp_api_key" value="<?php echo $api_key; ?>">
    </div>
    <?php
}

function mailing_list_settings_page_html() {
	// check user capabilities
	if (!current_user_can('manage_options')) {
		return;
	}
	?>

    <div class="wrap">
        <?php settings_errors();?>
        <form method="POST" action="options.php">
		    <?php settings_fields('mailing-list-settings-page'); ?>
		    <?php do_settings_sections('mailing-list-settings-page') ?>
		    <?php submit_button(); ?>
        </form>
    </div>
    <?php
}


function register_mailing_list_widget() {
	register_widget( 'WP_Widget_MailingList' );
}
add_action( 'widgets_init', 'register_mailing_list_widget' );


require_once( plugin_dir_path( __FILE__ ) . 'plugin-update-checker/plugin-update-checker.php' );
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://aybe-music-theme.s3.amazonaws.com/plugins/mailing-list.meta.json',
	__FILE__, // Full path to the main plugin file or functions.php.
	'aybe-mailing-list'
);

?>