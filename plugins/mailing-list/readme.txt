=== Plugin Name ===
Tags: mailing list
Requires at least: 5.4
Tested up to: 5.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

This plugin handles mailing list signup for the Ay.Be Music WordPress theme.

== Description ==

This plugin handles mailing list signup for the Ay.Be Music WordPress theme.

== Changelog ==

= 1.0 =
* Initial version of the Ay.Be Music mailing list signup plugin