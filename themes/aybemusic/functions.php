<?php

/*
 * Exit if accessed directly.
 */
defined( 'ABSPATH' ) || exit;

/**Includes reqired resources here**/
define('AYBE_TEMPLATE_DIR_URI', get_template_directory_uri());
define('AYBE_TEMPLATE_DIR', get_template_directory());
define('AYBE_THEME_FUNCTIONS_PATH', AYBE_TEMPLATE_DIR . '/functions');
define('AYBE_THEME_WIDGETS_PATH', AYBE_TEMPLATE_DIR . '/widgets');

require_once( AYBE_THEME_FUNCTIONS_PATH . '/mobile-menu-walker.php' );
require get_template_directory() . '/functions/customizer.php';


/*
 * Add CSS and JS files to WordPress
 */
add_action( 'wp_enqueue_scripts', function () {
    // Bootstrap CSS (must be first loaded CSS)
    wp_register_style(
        'bootstrap',
        get_template_directory_uri().'/assets/bootstrap/css/bootstrap.min.css',
        array(),
        '0.1.0',
        'all'
    );
    wp_enqueue_style( 'bootstrap' );

    // jQuery JS for Bootstrap
    wp_register_script(
        'jquery-for-bootstrap',
        // get_template_directory_uri().'/assets/jquery/jquery-3.4.1.min.js',
        get_template_directory_uri().'/assets/jquery/jquery-2.1.4.min.js',
        '',
        // '3.4.1-s',
        '2.1.4-s',
        false
    );
    wp_enqueue_script( 'jquery-for-bootstrap' );

    // Popper.js for Bootstrap
    wp_register_script(
        'popper-for-bootstrap',
        get_template_directory_uri().'/assets/popper/popper.min.js',
        '',
        '2.1.2',
        true
    );
    wp_enqueue_script( 'popper-for-bootstrap' );

    // Bootstrap JS
    wp_register_script(
        'bootstrap',
        get_template_directory_uri().'/assets/bootstrap/js/bootstrap.min.js',
        '',
        '3.4.1-s',
        false
    );
    wp_enqueue_script( 'bootstrap' );

    // Font-Awesome CSS
    wp_register_style(
        'fontawesome',
        get_template_directory_uri().'/assets/fontawesome/css/all.min.css',
        array(),
        '5.12.1',
        'all'
    );
    wp_enqueue_style( 'fontawesome' );

    // Font-Awesome JS
    wp_register_script(
        'fontawesome',
        get_template_directory_uri().'/assets/fontawesome/js/all.min.js',
        '',
        '5.12.1',
        false
    );
    wp_enqueue_script( 'fontawesome' );

    // Main CSS
    wp_register_style(
        'aybemusic',
        get_template_directory_uri().'/style.css',
        array(),
        '0.1.0',
        'all'
    );
    wp_enqueue_style( 'aybemusic' );

    // Main JS
    wp_register_script(
        'aybemusic',
        get_template_directory_uri().'/assets/js/custom.js',
        '',
        '0.1.0',
        true
    );
    wp_enqueue_script( 'aybemusic' );
} );

/*
 * Add menu support
 */
add_theme_support( 'menus' );
function aybe_register_menus() {
    register_nav_menus(
        array(
            'top-menu'    => __( 'Top Menu', 'aybemusic' ),
            'social-menu' => __( 'Social Links Menu', 'aybemusic' )
        )
    );
}
add_action( 'init', 'aybe_register_menus' );


/*
 * Screenshots
 */
function aybe_get_page_screenshots_from_menu( $menu, $width ) {
    $menu_items = wp_get_nav_menu_items( $menu );
    
    foreach ($menu_items as $item) {
        $url = str_replace( 'https://localhost/aybemusic', 'https://aybemusic.com', $item->url );
        $seed = date('m/d/yy');
        $image_url = 'http://s.wordpress.com/mshots/v1/' . urlencode( esc_url( $url ) ) . '?w=1600' . '$seed=x' . $seed;
        
        $split = explode( '/', $url );
        if ( $split[count($split) - 1] === '' ) {
            array_pop( $split );
        }
        $slug = $split[count($split) - 1];

        echo( '<img id="imgScreenshot-' . $slug . '" class="door-screenshot imgScreenshot-' . $slug . '" alt="' . $slug . ' screenshot" src="' . $image_url . '" />' );
    }
}


/*
 * Template functions
 */
function aybe_get_site_logo_left() {
?>
    <div class="d-none d-xl-flex col-xl-3 my-auto">
        <div class="row">
            <div class="col">
                <a href="<?php echo(get_home_url()); ?>"><img alt="AYBE Logo" src="<?php echo get_theme_mod('aybe_theme_logo_image'); ?>" /></a>
            </div>
        </div>
    </div>
<?php
}

function aybe_get_door_middle_xl( $show = true ) {
?>
    <div class="d-none d-xl-flex col-6 splash-content-col" <?php if ( !$show ) echo('style="display:none;"'); ?>>
        <div class="door-trim">
            <div class="door-vine"></div>
            <div class="back-door">
                <?php
                    aybe_get_page_screenshots_from_menu( 'top-menu', 1600 );
                ?>
                <div class="door-left"></div>
                <div class="door-right"></div>
            </div>
        </div>
    </div>
<?php
}

function aybe_get_door_middle_md_lg( $show = true ) {
?>
    <div class="row h-100 splash-content-row d-none d-md-flex d-xl-none">
        <div class="col-12 splash-content-col" <?php if ( !$show ) echo('style="display:none;"'); ?>>
            <div class="door-trim">
                <div class="door-vine"></div>
                <div class="back-door">
                    <?php
                        aybe_get_page_screenshots_from_menu( 'top-menu', 1600 );
                    ?>
                    <div class="door-left"></div>
                    <div class="door-right"></div>
                </div>
            </div>
        </div>
    </div>
<?php
}

function aybe_get_menu_right() {
?>
    <div class="d-none d-xl-block col-xl-3 my-auto">
        <div class="row">
            <div class="col text-right">
            <?php
                wp_nav_menu(
                    array(
                        'theme_location'  => 'top-menu',
                        'container_class' => 'side-menu'
                    )
                );
            ?>
            </div>
        </div>
    </div>
<?php
}

function aybe_get_door_mobile( $show = true ) {
?>
    <div class="row d-block d-md-none">
        <div class="col-12 splash-content-col-mobile" <?php if ( !$show ) echo('style="display:none;"'); ?>>
            <div class="door-trim-mobile">
                <div class="door-vine-mobile"></div>
                <div class="back-door-mobile">
                    <?php
                        aybe_get_page_screenshots_from_menu( 'top-menu', 1600 );
                    ?>
                    <div class="door-left-mobile"></div>
                    <div class="door-right-mobile"></div>
                </div>
            </div>
        </div>
    </div>
<?php
}


/*
 * Register widgetized areas
 */
if ( function_exists('register_sidebar') ) {
    register_sidebar(
        array(
            'id'            => 'sidebar-modal-widgets',
            'name'          => 'Modal Widgets',
            'before_widget' => '<div class="widgetizedArea">',
            'after_widget'  => '</div>'
        )
    );
}


/*
 * Utilities
 */
function aybe_log_debug_msg( $msg, $name = '' ) {
    if ( WP_DEBUG === true ) {
        $trace = debug_backtrace();
        $name == ('' == $name) ? $trace[1]['function'] : $name;

        $msg = print_r( $msg, true );
        $log = $name . ' | ' . $msg;

        error_log( $log );
    }
}


add_theme_support( 'post-thumbnails' );


require_once( plugin_dir_path( __FILE__ ) . 'plugin-update-checker/plugin-update-checker.php' );
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://aybe-music-theme.s3.amazonaws.com/themes/aybemusic.meta.json',
	__FILE__, // Full path to the main plugin file or functions.php.
	'aybemusic-theme'
);