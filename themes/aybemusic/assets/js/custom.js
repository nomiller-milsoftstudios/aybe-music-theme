var faCustomIcons = [
    {
        prefix: 'fac',
        iconName: 'key',
        icon: [416.03, 151.69, [], 'e001', 'M 257.43 90.48 c -1.77 -2.31 -3.51 -2.64 -5.94 -2.65 c -35.56 -0.2 -124.86 -1.07 -133.93 -1.39 c -5.35 -0.19 -9.32 2.09 -11.91 7 a 81.89 81.89 0 0 0 -3.33 7.53 c -1.56 3.82 -2.88 7.75 -4.59 11.49 c -2.86 6.21 -6.72 11.68 -12.21 15.42 A 50.15 50.15 0 0 1 58 137.07 c -13.78 0.26 -24.85 -6.43 -33.13 -17.94 C 17.34 108.73 14 96.36 12.64 83.49 c -1.44 -14.12 1.07 -27.7 6.43 -40.7 c 5.6 -13.57 15.06 -22.89 28.11 -27 c 15 -4.68 28.84 -0.6 40.75 10.06 C 94.76 32 97.23 37 100.48 45.75 a 63.35 63.35 0 0 0 5.29 11.08 a 9.52 9.52 0 0 0 9 4.7 c 17 -0.59 110 1 138.81 0.61 a 5 5 0 0 0 4.29 -2.09 c 5.32 -6.68 13.74 -7.39 19.16 -2.56 a 2.54 2.54 0 0 0 0.86 0.4 c 2.62 -0.93 5 -2 7.52 -2.62 a 8.93 8.93 0 0 1 9.6 3 a 5.87 5.87 0 0 0 5.46 2.29 a 50.46 50.46 0 0 1 5 0.23 c 11.93 0.33 23.87 0.75 35.8 0.93 c 11.54 0.17 23.1 -0.09 34.64 0.2 a 31 31 0 0 1 10.22 2.2 c 6.32 2.43 8.28 11.1 4 16.61 c -2.76 3.55 -6 6.2 -10.42 6.61 c -3.34 0.32 -6.72 0.23 -10.09 0.22 c -1.92 0 -3.84 -0.2 -6 -0.32 c -0.38 2.86 -0.6 5.48 -1.08 8 c -0.74 3.88 -2.33 5.41 -6 6.06 c -1.38 0.24 -2.79 0.32 -4.42 0.51 c -0.28 3.22 -0.55 6.32 -0.83 9.5 c 1.52 0.26 2.54 0.47 3.55 0.61 c 6.65 0.93 8.49 3.14 8.5 10.34 c 0 4.46 0 8.91 -0.09 13.37 c -0.07 2.64 -0.34 5.27 -0.66 7.89 c -0.17 1.44 -0.75 2.68 -2.43 2.7 c -5.32 0.06 -10.64 0.16 -16 0.2 c -1.87 0 -2.9 -0.91 -2.8 -3.07 c 0.06 -1.36 0 -2.74 0 -4.42 h -8.74 c -0.11 1.42 -0.17 2.66 -0.3 3.88 c -0.3 3.18 -0.95 4 -3.82 4 c -4.41 0 -8.81 -0.26 -13.22 -0.33 c -2 0 -2.79 -1.21 -2.9 -3.12 c -0.45 -8.05 -0.91 -16.1 -1.28 -24.16 a 5.28 5.28 0 0 1 4.17 -5.72 c 1.72 -0.52 3.44 -1 5.05 -1.49 v -8.95 c -3 -1.37 -6.5 -1.87 -8.17 -4 s -1.85 -6.09 -2.76 -9.6 c -3.13 0 -6.49 0.07 -9.84 0 a 4 4 0 0 0 -3.9 2 c -2.17 3.27 -5.26 4.66 -9 4.92 a 11.35 11.35 0 0 1 -9 -3.41 C 269.42 97.89 262.58 97.17 257.43 90.48 Z M 39.4 64.24 c 0.69 1.34 1.42 2.66 2.2 3.94 c 3.12 5.09 3.14 10.2 0 15.24 A 27.6 27.6 0 0 0 37.15 96.6 C 36 113.14 50.9 125.87 65.53 120.7 c 4.34 -1.54 8.41 -3.61 11.34 -7.67 a 20.59 20.59 0 0 0 3.06 -18.92 c -1.13 -3.64 -3.12 -7 -4.81 -10.41 c -0.73 -1.47 -1.73 -2.78 -2.57 -4.19 a 6.84 6.84 0 0 1 0 -7.46 C 74 69.6 75.66 67.24 77 64.71 c 2.8 -5.15 4.59 -10.62 3.66 -16.72 c -1.55 -10.23 -7 -14.54 -15.53 -16.91 a 50.28 50.28 0 0 0 -6.14 -1 C 41.5 29.11 32.65 50.94 39.4 64.24 Z']
    }
];

for (var i = 0; i < faCustomIcons.length; i++) {
    FontAwesome.library.add(
        faCustomIcons[i]
    );
}

$(document).ready(function() {
    $('.side-menu a, .mobile-menu a').click(function(e) {
        e.preventDefault();

        var url = $(this).attr('href');

        var splitUrl = url.split('/');
        if (splitUrl[splitUrl.length - 1] === '') {
            splitUrl.pop();
        }
        var slug = splitUrl[splitUrl.length - 1];

        $('.mobile-menu').removeClass('open');

        $('.imgScreenshot-' + slug).addClass('door-screenshot-open');
        
        $('.splash-content-col').addClass('show-doors');

        setTimeout(function() {
            $('.door-left').filter(':visible').addClass('door-left-open');
            $('.door-right').filter(':visible').addClass('door-right-open');

            $('.door-trim').filter(':visible').addClass('door-trim-open');

            window.location.href = url;
        }, 250);
    });

    $('#mailingListForm').submit(function(e) {
        e.preventDefault();
        
        var email = $('#txtMailingListEmail').val();

        if (!email) {
            $('#txtMailingListEmail').addClass('is-invalid');
        }
        else {
            $('#mlEmail').val(email);
            $('#mailingListModal').modal('show');
            $('#txtMailingListEmail').val('');
        }
    });

    $('#txtMailingListEmail').focus(function(e) {
        $(this).removeClass('is-invalid');
    });

    $('.side-menu a').on('mouseenter', function(e) {
        $(this).prepend('<i class="fac fa-key"></i>');
    });

    $('.side-menu a').on('mouseleave', function(e) {
        $('.side-menu a .fa-key').remove();
    });

    $('.collapse.music-collapse').on('show.bs.collapse', function(e) {
        var $card = $(this).closest('.music-tile');
        var $open = $($(this).data('parent')).find('.collapse.show');
      
        var additionalOffset = 0;
        if ($card.prevAll().filter($open.closest('.card')).length !== 0) {
            additionalOffset =  $open.height();
        }
        $('.page-content-col').animate({
            scrollTop: $card.offset().top - additionalOffset
        }, 250);
    });

    $('[data-toggle="offcanvas"]').on('click', function () {
        $('.offcanvas-collapse').toggleClass('open');
    });

    $('.et_pb_toggle_title').on('click', function() {
        $(this).toggleClass('open');
    });
});