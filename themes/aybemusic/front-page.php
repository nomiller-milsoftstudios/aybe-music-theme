<?php 
/*
 * Template Name: Front Page
 * Description: Template for the front page
 * Version: 0.1.0
 */

get_header(); ?>

<div class="row h-100 splash-content-row d-none d-xl-flex">
    <?php
        aybe_get_site_logo_left();
        aybe_get_door_middle_xl();
        aybe_get_menu_right();
    ?>
</div>

<?php
    aybe_get_door_middle_md_lg();
    aybe_get_door_mobile();
    get_footer();
?>