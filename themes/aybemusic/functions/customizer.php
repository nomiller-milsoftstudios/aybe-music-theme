<?php

function aybe_theme_customizer_settings($wp_customize) {

    // Add a section for the Footer settings
    $wp_customize->add_section('aybe_theme_footer', array(
        'title' => 'Footer',
        'description' => '',
        'priority' => 120,
    ));


    // -------------------------------------
    // Logo image
    // -------------------------------------

    // Add a setting for the Logo image
    $wp_customize->add_setting('aybe_theme_logo_image', array(
        'sanitize_callback' => 'esc_url_raw'
    ));

    // Add a control to upload/choose the image
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'aybe_theme_logo_image',
        array(
            'label' => 'Upload Logo',
            'priority' => 20,
            'section' => 'title_tagline',
            'settings' => 'aybe_theme_logo_image',
            'button_labels' => array(
                'select' => 'Select Logo',
                'remove' => 'Remove Logo',
                'change' => 'Change Logo',
            )
        )
    ));

    
    // -------------------------------------
    // Copyright
    // -------------------------------------

    // Add a setting for the Copyright text
    $wp_customize->add_setting('aybe_theme_copyright');

    // Add a control to change the text
    $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'aybe_theme_copyright',
        array(
            'label' => 'Copyright',
            'section' => 'aybe_theme_footer',
            'settings' => 'aybe_theme_copyright',
        )
    ));
}

add_action('customize_register', 'aybe_theme_customizer_settings');
    
?>