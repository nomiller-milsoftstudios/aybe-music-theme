<?php

/************* Music custom post type ************************/
function aybe_music_type() {
    register_post_type( 'aybe_music',
        array(
            'labels' => array(
                'name' => __('Music', 'aybemusic'),
                'add_new' => __('Add New', 'aybemusic'),
                'add_new_item' => __('Add New Music','aybemusic'),
                'edit_item' => __('Edit','aybemusic'),
                'new_item' => __('Edit Music','aybemusic'),
                'all_items' => __('All Music','aybemusic'),
                'view_item' => __('View Music','aybemusic'),
                'search_items' => __('Search Music','aybemusic'),
                'not_found' =>  __('No music found','aybemusic'),
                'not_found_in_trash' => __('No music found in trash','aybemusic'), 
            ),
            'supports' => array('title','thumbnail','page-attributes'),
            'show_in_nav_menus' => false,
            'public' => true,
            'menu_icon' => 'dashicons-format-audio',
        )
	);
}
add_action( 'init', 'aybe_music_type' );

function aybe_music_credits_mb(){
  global $post;
  $custom = get_post_custom($post->ID);
  $credits = array_key_exists( 'credits', $custom ) ? $custom['credits'][0] : '';
?>

  <p>
    <label>Credits:</label><br />
    <textarea cols="50" rows="5" name="credits"><?php echo $credits; ?></textarea>
  </p>

<?php
}

function aybe_music_listen_now_meta_mb() {
    global $post;
    $custom = get_post_custom($post->ID);
    $listen_now_apple_music_link   = array_key_exists( 'listen_now_apple_music_link', $custom )? $custom['listen_now_apple_music_link'][0] : '';
    $listen_now_tidal_link         = array_key_exists( 'listen_now_tidal_link', $custom )? $custom['listen_now_tidal_link'][0] : '';
    $listen_now_soundcloud_link    = array_key_exists( 'listen_now_soundcloud_link', $custom )? $custom['listen_now_soundcloud_link'][0] : '';
    $listen_now_spotify_link       = array_key_exists( 'listen_now_spotify_link', $custom )? $custom['listen_now_spotify_link'][0] : '';
    $listen_now_genius_lyrics_link = array_key_exists( 'listen_now_genius_lyrics_link', $custom )? $custom['listen_now_genius_lyrics_link'][0] : '';
?>

    <table cellpadding="4" cellspacing="0" style="border:none;">
        <tr>
            <td>Apple Music:</td>
            <td><input name="listen_now_apple_music_link" value="<?php echo $listen_now_apple_music_link; ?>" size="100" /></td>
        </tr>
        <tr>
            <td>Tidal:</td>
            <td><input name="listen_now_tidal_link" value="<?php echo $listen_now_tidal_link; ?>" size="100" /></td>
        </tr>
        <tr>
            <td>Soundcloud:</td>
            <td><input name="listen_now_soundcloud_link" value="<?php echo $listen_now_soundcloud_link; ?>" size="100" /></td>
        </tr>
        <tr>
            <td>Spotify:</td>
            <td><input name="listen_now_spotify_link" value="<?php echo $listen_now_spotify_link; ?>" size="100" /></td>
        </tr>
        <tr>
            <td>Genius Lyrics:</td>
            <td><input name="listen_now_genius_lyrics_link" value="<?php echo $listen_now_genius_lyrics_link; ?>" size="100" /></td>
        </tr>
    </table>

<?php
}

function aybe_music_buy_now_meta_mb() {
    global $post;
    $custom = get_post_custom($post->ID);
    $buy_now_itunes_link      = array_key_exists( 'buy_now_itunes_link', $custom )? $custom['buy_now_itunes_link'][0] : '';
    $buy_now_google_play_link = array_key_exists( 'buy_now_google_play_link', $custom )? $custom['buy_now_google_play_link'][0] : '';
    $buy_now_amazon_link      = array_key_exists( 'buy_now_amazon_link', $custom )? $custom['buy_now_amazon_link'][0] : '';
?>

    <table cellpadding="4" cellspacing="0" style="border:none;">
        <tr>
            <td>iTunes:</td>
            <td><input name="buy_now_itunes_link" value="<?php echo $buy_now_itunes_link; ?>" size="100" /></td>
        </tr>
        <tr>
            <td>Google Play:</td>
            <td><input name="buy_now_google_play_link" value="<?php echo $buy_now_google_play_link; ?>" size="100" /></td>
        </tr>
        <tr>
            <td>Amazon:</td>
            <td><input name="buy_now_amazon_link" value="<?php echo $buy_now_amazon_link; ?>" size="100" /></td>
        </tr>
    </table>
    
<?php
}

function aybe_music_edit_columns($columns){
    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => 'Song Title',
        'credits' => 'Credits',
        'order' => 'Order',
        'status' => 'Status',
        'date' => 'Date',
    );

    return $columns;
}
function aybe_music_custom_columns($column){
    global $post;

    switch ($column) {
        case 'title':
            echo( $post->post_title );
            break;
        case 'credits':
            $custom = get_post_custom();
            echo $custom['credits'][0];
            break;
        case 'order':
            echo( $post->menu_order );
            break;
        case 'status':
            echo( $post->post_status );
            break;
        case 'date':
            echo ( $post->post_date );
            break;
    }
}
add_action('manage_posts_custom_column', 'aybe_music_custom_columns');
add_filter('manage_edit-aybe_music_columns', 'aybe_music_edit_columns');


/************* All post types ************************/
function aybe_admin_init(){
    // Music type
    add_meta_box( 'credits_meta', 'Credits', 'aybe_music_credits_mb', 'aybe_music', 'normal', 'low');
    add_meta_box( 'listen_now_meta', 'Listen Now Links', 'aybe_music_listen_now_meta_mb', 'aybe_music', 'normal', 'low');
    add_meta_box( 'buy_now_meta', 'Buy Now Links', 'aybe_music_buy_now_meta_mb', 'aybe_music', 'normal', 'low');

    // Video type
    add_meta_box( 'watch_now_meta', 'Watch Now Links', 'aybe_video_watch_now_meta_mb', 'aybe_video', 'normal', 'low');
}
add_action( 'admin_init', 'aybe_admin_init' );

function aybe_save_details(){
    global $post;
    if ($post == null)
        return;

    if ( $post->post_type == 'aybe_music' ) {
        update_post_meta($post->ID, 'credits', $_POST['credits']);

        // Listen now links
        $ln_apple_music = trim( $_POST['listen_now_apple_music_link'] );
        update_post_meta( $post->ID, 'listen_now_apple_music_link', $ln_apple_music );
        $ln_tital = trim( $_POST['listen_now_tidal_link'] );
        update_post_meta( $post->ID, 'listen_now_tidal_link', $ln_tital );
        $ln_soundcloud = trim( $_POST['listen_now_soundcloud_link'] );
        update_post_meta( $post->ID, 'listen_now_soundcloud_link', $ln_soundcloud );
        $ln_spotify = trim( $_POST['listen_now_spotify_link'] );
        update_post_meta( $post->ID, 'listen_now_spotify_link', $ln_spotify );
        $ln_genius = trim( $_POST['listen_now_genius_lyrics_link'] );
        update_post_meta( $post->ID, 'listen_now_genius_lyrics_link', $ln_genius );

        $has_listen_now = 
            $ln_apple_music != '' ||
            $ln_tital       != '' ||
            $ln_soundcloud  != '' ||
            $ln_spotify     != '' ||
            $ln_genius      != '';
        update_post_meta( $post->ID, 'has_listen_now', $has_listen_now );

        // Buy now links
        $bn_itunes = trim( $_POST['buy_now_itunes_link'] );
        update_post_meta( $post->ID, 'buy_now_itunes_link', $bn_itunes );
        $bn_google_play = trim( $_POST['buy_now_google_play_link'] );
        update_post_meta( $post->ID, 'buy_now_google_play_link', $bn_google_play );
        $bn_amazon = trim( $_POST['buy_now_amazon_link'] );
        update_post_meta( $post->ID, 'buy_now_amazon_link', $bn_amazon );

        $hasBuyNow = 
            $bn_itunes      != '' ||
            $bn_google_play != '' ||
            $bn_amazon      != '';
        update_post_meta( $post->ID, 'has_buy_now', $hasBuyNow );
    }
}
add_action('save_post', 'aybe_save_details');

?>
