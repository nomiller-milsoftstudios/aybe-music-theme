            <div class="row fixed-bottom footer d-none d-xl-flex">
                <div class="col-6 text-left subscribe pl-4 pb-4 pr-4 pt-2">
                    <form id="mailingListForm" method="post">
                        <div class="d-inline-block">
                            <div>
                                <label for="txtMailingListEmail">Join the Mailing List</label>
                            </div>
                            <div>
                                <input type="text" id="txtMailingListEmail" name="email" placeholder="Email..." />
                            </div>
                        </div>
                        <div class="d-inline-block ml-3">
                            <input type="submit" class="site-button" value="Submit" />
                        </div>
                    </form>
                </div>

                <div class="col-6 text-right copyright p-4">
                    <?php
                    if ( get_theme_mod( 'aybe_theme_copyright' ) ) : ?>
                    <div class="copy-line">&copy; <?php echo get_theme_mod( 'aybe_theme_copyright' ); ?></div>
                    <?php
                    else: ?>
                    <div class="copy-line">&copy; Ashley Barrett</div>
                    <?php endif; ?>

                    <div class="created-by">Created by Coalesced Data Solutions, LLC</div>
                </div>
            </div>

            <?php // Mobile Footer Start ?>
            <div class="row text-center d-block d-xl-none">
                <div class="col-12 text-center subscribe p-2">
                    <form id="mailingListForm" method="post">
                        <div class="d-inline-block">
                            <div class="text-left">
                                <label for="txtMailingListEmail">Join the Mailing List</label>
                            </div>
                            <div class="text-left">
                                <input type="text" id="txtMailingListEmail" name="email" placeholder="Email..." />
                            </div>
                        </div>
                        <div class="d-inline-block ml-3 text-left">
                            <input type="submit" class="site-button" value="Submit" />
                        </div>
                    </form>
                </div>
            </div>
            <div class="align-bottom d-block d-xl-none">
                <div class="row text-center mt-3">
                    <div class="col-12">
                        <?php
                            wp_nav_menu(
                                array(
                                    'theme_location'  => 'social-menu',
                                    'container_class' => 'social-menu'
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-12 text-center copyright p-2">
                        <?php
                        if ( get_theme_mod( 'aybe_theme_copyright' ) ) : ?>
                        <div class="copy-line">&copy; <?php echo get_theme_mod( 'aybe_theme_copyright' ); ?></div>
                        <?php
                        else: ?>
                        <div class="copy-line">&copy; Ashley Barrett</div>
                        <?php endif; ?>
                        
                        <div class="created-by">Created by Coalesced Data Solutions, LLC</div>
                    </div>
                </div>
            </div
            <?php // Mobile Footer End ?>
        </div>

        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("sidebar-modal-widgets") ) :
        ?>
        <?php endif;?>

        <?php wp_footer(); ?>
    </body>
</html>