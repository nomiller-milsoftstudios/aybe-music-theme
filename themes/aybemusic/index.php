<?php get_header(); ?>

<div class="row page-content-row">
    <?php 
        aybe_get_site_logo_left();
    ?>     

    <div class="col-xs-12 col-xl-6 page-content-col">
        <!-- <div class="row">
            <div class="col">
                <h2><?php the_title(); ?></h2>
            </div>
        </div> -->
        <div class="row">
            <div class="col">
            <?php
                while ( have_posts() ) : the_post();
                    the_content();
                endwhile;
                wp_reset_query();
            ?>
            </div>
        </div>
    </div>

    <?php aybe_get_menu_right(); ?>
</div>

<?php
    get_footer();
?>