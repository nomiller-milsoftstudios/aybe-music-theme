<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=yes" />
        <meta http-equiv="ScreenOrientation" content="autoRotate:disabled">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <?php wp_head(); ?>

        <title><?php echo wp_title(); ?></title>
    </head>
    <body <?php body_class(); ?>>
        <header>

        </header>

        <div class="text-right fixed-top d-none d-xl-block">
            <?php
                wp_nav_menu(
                    array(
                        'theme_location'  => 'social-menu',
                        'container_class' => 'social-menu'
                    )
                );
            ?>
        </div>

        <nav id="mobileMenu" class="navbar d-xl-none navbar-toggleable-sm navbar-light bg-transparent">
            <a class="navbar-brand mx-auto" href="<?php echo(get_home_url()); ?>">
                <img alt="AYBE Logo" class="mobile-logo mx-auto" src="<?php echo get_theme_mod('aybe_theme_logo_image'); ?>" />
            </a>

            <button class="navbar-toggler p-0 border-0 d-xl-none" type="button" data-toggle="offcanvas">
                <span class="navbar-toggler-icon"></span>
            </button>
            <?php
                wp_nav_menu(
                    array(
                        'theme_location'  => 'top-menu',
                        'container_class' => 'mobile-menu navbar-collapse offcanvas-collapse d-xl-none',
                        'menu_class'      => 'navbar-nav mx-auto',
                        'walker'          => new Mobile_Nav_Menu_Walker()
                    )
                );
            ?>
        </nav>

        <div id="content" class="container-fluid h-100">