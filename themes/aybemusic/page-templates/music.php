<?php 
/*
 * Template Name: Music
 * Description: Template for the Music page
 */

get_header(); ?>

<div class="row page-content-row">
    <?php 
        aybe_get_site_logo_left();
    ?>    

    <div class="col-xs-12 col-xl-6 page-content-col">
        <div class="row">
            <div class="col-12">
                <div class="row">
                
            <?php
                while ( have_posts() ) : the_post();
                    the_content();
                endwhile;
                wp_reset_query();

                $music = get_posts( array(
                    'post_type' => 'aybe_music',
                    'orderby'   => 'menu_order',
                    'order'     => 'ASC'
                ));

                if ( count( $music ) > 0 ) {
                    $num = 0;
                    $numCols = 2;
                    $store_logo_dir = get_template_directory_uri() . '/images/store_logos/';

                    foreach ( $music as $m ) {
                        $custom = get_post_custom($m->ID);
            ?>

                <div class="col-12 col-lg-6 mt-3">
                    <div class="music-tile">
                        <div class="music-thumbnail">
                            <img class="img-fluid" src="<?php echo( get_the_post_thumbnail_url( $m ) ); ?>" alt="<?php echo( $m->post_title ); ?>" />
                        </div>
                        <?php
                            if ( $custom['has_listen_now'][0] ) {
                        ?>
                        
                        <div class="accordion" id="listen-now-<?php echo( $m->ID ); ?>">
                            <div class="card" style="border:none;">
                                <div class="card-header x-now-header" id="listen-now-header-<?php echo( $m->ID ); ?>">
                                    <div class="row" data-toggle="collapse" data-target="#listen-now-body-<?php echo( $m->ID ); ?>" aria-expanded="false" aria-controls="listen-now-body-<?php echo( $m->ID ); ?>">
                                        <div class="col-8">Listen Now</div>
                                        <div class="col-4 text-right"><span class="icon"></span></div>
                                    </div>
                                </div>
                                <div id="listen-now-body-<?php echo( $m->ID ); ?>" class="collapse music-collapse" aria-labelledby="listen-now-header-<?php echo( $m->ID ); ?>" data-parent="#listen-now-<?php echo( $m->ID ); ?>">
                                    <div class="card-body x-now-body text-center">
                                        <ul>
                                            <?php
                                            if ( $custom['listen_now_apple_music_link'][0] != '' )
                                                echo( '<li><a href="' . $custom['listen_now_apple_music_link'][0] . '" target="_blank"><img src="' . $store_logo_dir . 'apple-music.svg" class="store-logo" alt="Apple Music" /></a></li></a>' );

                                            if ( $custom['listen_now_tidal_link'][0] != '' )
                                                echo( '<li><a href="' . $custom['listen_now_tidal_link'][0] . '" target="_blank"><img src="' . $store_logo_dir . 'tidal.svg" class="store-logo" alt="Tidal" /></a></li>' );

                                            if ( $custom['listen_now_soundcloud_link'][0] != '' )
                                                echo( '<li><a href="' . $custom['listen_now_soundcloud_link'][0] . '" target="_blank"><img src="' . $store_logo_dir . 'soundcloud.svg" class="store-logo" alt="Soundcloud" /></a></li>' );

                                            if ( $custom['listen_now_spotify_link'][0] != '' )
                                                echo( '<li><a href="' . $custom['listen_now_spotify_link'][0] . '" target="_blank"><img src="' . $store_logo_dir . 'spotify.svg" class="store-logo" alt="Spotify" /></a></li>' );

                                            if ( $custom['listen_now_genius_lyrics_link'][0] != '' )
                                                echo( '<li><a href="' . $custom['listen_now_genius_lyrics_link'][0] . '" target="_blank"><img src="' . $store_logo_dir . 'genius.png" class="store-logo" alt="Genius" /></a></li>' );
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                            }

                            if ( $custom['has_buy_now'][0] ) {
                        ?>

                        <div class="accordion" id="buy-now-<?php echo( $m->ID ); ?>" <?php if( $custom['listen_now_apple_music_link'][0] ) echo( 'style="border-top:2px solid #fff;"' ); ?>>
                            <div class="card" style="border:none;">
                                <div class="card-header x-now-header" id="buy-now-header-<?php echo( $m->ID ); ?>">
                                    <div class="row" data-toggle="collapse" data-target="#buy-now-body-<?php echo( $m->ID ); ?>" aria-expanded="false" aria-controls="buy-now-body-<?php echo( $m->ID ); ?>">
                                        <div class="col-8">Buy Now</div>
                                        <div class="col-4 text-right"><span class="icon"></span></div>
                                    </div>
                                </div>
                                <div id="buy-now-body-<?php echo( $m->ID ); ?>" class="collapse music-collapse" aria-labelledby="buy-now-header-<?php echo( $m->ID ); ?>" data-parent="#buy-now-<?php echo( $m->ID ); ?>">
                                    <div class="card-body x-now-body text-center">
                                        <ul>
                                            <?php
                                            if ( $custom['buy_now_itunes_link'][0] != '' )
                                                echo( '<li><a href="' . $custom['buy_now_itunes_link'][0] . '" target="_blank"><img src="' . $store_logo_dir . 'itunes.svg" class="store-logo" alt="iTunes Store" /></a></li>' );

                                            if ( $custom['buy_now_google_play_link'][0] != '' )
                                                echo( '<li><a href="' . $custom['buy_now_google_play_link'][0] . '" target="_blank"><img src="' . $store_logo_dir . 'google-play.svg" class="store-logo" alt="Google Play" /></a></li>' );

                                            if ( $custom['buy_now_amazon_link'][0] != '' )
                                                echo( '<li><a href="' . $custom['buy_now_amazon_link'][0] . '" target="_blank"><img src="' . $store_logo_dir . 'amazon-music.svg" class="store-logo" alt="Amazon Music" /></a></li>' );
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                            }
                        ?>
                    </div>
                </div>
            <?php

                        $num++;
                    }
                }
            ?>
                </div>
            </div>
        </div>
    </div>

    <?php aybe_get_menu_right(); ?>
</div>

<?php
    get_footer();
?>